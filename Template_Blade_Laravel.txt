﻿Template Blade Laravel


1.BlogController.php


<?php 
namespace App\Http\Controllers;


use Illuminate\Http\Request;


class BlogController extends Controller
{
    public function blog()
    {
        return view('blog');
    }
    public function tentang() 
    {
        return view('tentang');
    }
    public function beranda() 
    {
        return view('beranda');
    }
}


2.beranda.blade.php


@extends('blog')


@section('title','beranda')


@section('content')
<h2>Selamat datang di halaman beranda!</h2>
<p>Ini adalah halaman beranda blog sederhana.</p>
@endsection


3.tentang.blade.php


@extends('blog')


@section('title','Tentang')


@section('content')
<h2>Selamat Datang!</h2>
<p>Ini adalah halaman tentang blog sederhana kami.</p>
@endsection


4.blog.blade.php


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
</head>
<body>
    <header>
        <h1>Selamat datang di Blog MAKN Ende</h1>
        <nav>
            <a href="/blog">BERANDA</a>
            <a href="/blog/tentang">TENTANG</a>
            <a href="https://makn-ende.sch.id/">KONTAK</a>
        </nav>
    </header>


    
   
    
    <br>
    
    <h3>@yield('title')</h3>


    <main>@yield('content')</main>
    


    <br>
    <br>
    <hr>
    <footer>
        <p>&copy; <a href="https://makn-ende.sch.id/">www.maknende.sch.id</a></p>
    </footer>
</body>
</html>


5.web.php


<?php


use App\Http\Controllers\BlogController;
use App\Http\Controllers\DosenController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/blog', [BlogController::class, 'beranda']);
Route::get('/blog/tentang', [BlogController::class, 'tentang']);